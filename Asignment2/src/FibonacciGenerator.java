import java.util.Scanner;

public class FibonacciGenerator {
 private static int calculateNthValue(int n) {
  if (n == 1 || n == 2) {
   return 1;
  } else {
   return calculateNthValue(n - 1) + calculateNthValue(n - 2);
  }

 }

 private static int validateN(String str) {
  while (!(str.matches("\\d+"))) {
   System.out.println("The input" + str + "is not a number");
   break;
  }
  System.out.println("The input " + str + " is a number");
  return Integer.parseInt(str);

 }

 public static void main(String[] args) {
  System.out.println("please input a number��");
  Scanner input = new Scanner(System.in);
  String str = input.nextLine();
  int n = validateN(str);
  System.out.println("The " + str + " value in the Fibonacci Sequence is:" + calculateNthValue(n));
 }
}